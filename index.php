<?php
session_start();
if($_GET['show_desktop_mode'] == 'true') {
    $_SESSION['desktopmode'] = 'true';
}else{
	$_SESSION['desktopmode'] = 'false';
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Детский развивающий центр от 7 месяцев до 7 лет в Астане</title>
	<meta name="description" content="Детский развивающий центр от 7 месяцев до 7 лет в Астане по доступным ценам."/>
	<meta name="keywords" content="детский развивающий центр, центр развития для детей, детский развивающий центр занятия, хорошие детские развивающие центры, детские развивающие центры астана, детский развивающий центр в астане"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php
	if($_SESSION['desktopmode'] == 'true') {
	    /* DESKTOP MODE */
	    ?>
	    <meta name="viewport" content="width=1024">
	    <?php
	}
	else if($_SESSION['desktopmode'] == 'false') {
	    /* MOBILE MODE */
	    ?>
	     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <?php
	} 
	else {
	    // DEFAULT
	    ?>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <?php
	}
	?>
	<link rel="shortcut icon" href="img/favicon/favicon.png">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <noindex><script async src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTE1MTE5O2xzdGF0cy5yZWZlcmVyKCl9O3ZhciBqcXVlcnlfbHN0YXRzPWZ1bmN0aW9uKCl7alFzdGF0Lm5vQ29uZmxpY3QoKTtsb2Fkc2NyaXB0KCJzdGF0c19hdXRvLmpzIixpbml0X2xzdGF0cyl9O2xvYWRzY3JpcHQoImpxdWVyeS0xLjEwLjIubWluLmpzIixqcXVlcnlfbHN0YXRzKQ=="></script></noindex>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '188856738143560');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=188856738143560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<script type="text/javascript">
		$(document).ready(function(){
			$("#selectCityModal").modal('show');
		});
		function changeCity(n) { 
		    document.getElementById("h_org-phone"+(n^1)).style.display = "none"; 
		    document.getElementById("h_org-phone"+n).style.display = "block"; 
		    document.getElementById("f_org-phone"+(n^1)).style.display = "none"; 
		    document.getElementById("f_org-phone"+n).style.display = "block"; 
		    document.getElementById("h_org-address"+(n^1)).style.display = "none"; 
		    document.getElementById("h_org-address"+n).style.display = "block"; 
		    document.getElementById("f_org-address"+(n^1)).style.display = "none"; 
		    document.getElementById("f_org-address"+n).style.display = "block"; 
		    document.getElementById("f_org-map"+(n^1)).style.display = "none"; 
		    document.getElementById("f_org-map"+n).style.display = "block"; 
		} 
	</script>
	<style>
	.contacts-orda {
		display: none; 
	}
	</style>
</head>

<body>
	<div id="selectCityModal" class="modal fade">
	    <div class="modal-dialog modal-sm">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title">Выберите город</h4>
	            </div>
	            <div class="modal-body">
					<a onclick="changeCity(0)"  data-dismiss="modal">Астана</a><br/>
	          		<a onclick="changeCity(1)"  data-dismiss="modal">Кызылорда</a>
	            </div>
	        </div>
	    </div>
	</div>
	<header class="main-head" style="background-image: url(img/header_bg.jpg);">
		<div class="top-line">
			
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-3">
						<a href="" class="logo"><img src="img/logo.png" alt="Creative Minds Group"></a><br/>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="col-lg-12">
						<nav class="main-mnu">
							<ul>
								<li><a href="#advantages" class="smoothScroll">ПРЕИМУЩЕСТВА</a></li>
								<li><a href="#programs" class="smoothScroll">ПРОГРАММЫ</a></li>
								<li><a href="#reviews" class="smoothScroll">ОТЗЫВЫ</a></li>
								<li><a href="#contacts" class="smoothScroll">КОНТАКТЫ</a></li>
							</ul>
						</nav>
						<div id="h_org-address0" class="contacts-astana">
							<div class="address visible-lg visible-md"><p>Ваш город: <a data-toggle="modal" href="#selectCityModal">Астана, Кошкарбаева 2, ЖК Highvill Astana, C2</a></p></div>
						</div>
						<div id="h_org-address1" class="contacts-orda">
							<div class="address visible-lg visible-md"><p>Ваш город: <a data-toggle="modal" href="#selectCityModal">Кызылорда, С. Сулейменова 70</a></p></div>
						</div>
					</div>	
					</div>
					<div id="h_org-phone0" class="contacts-astana">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-9">
							<div class="phone-wrap" style="float:right;">
								<div class="phone">+7 (771) 525 01 58</div>
								<div class="phone">+7 (7172) 266 588</div>
								<a href="#" class="toggle-mnu hidden-lg"><span></span></a>
								<br/>
								<a href="#callback" class="button-call">Заказать звонок</a>
							</div>
						</div>
					</div>
					<div id="h_org-phone1" class="contacts-orda">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-9">
							<div class="phone-wrap" style="float:right;">
								<div class="phone">+7 (701) 755 68 00</div>
								<a href="#" class="toggle-mnu hidden-lg"><span></span></a>
								<br/>
								<a href="#callback" class="button-call">Заказать звонок</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>

		<div class="header-wrap">

			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<h1 class="align-center">ДЕТСКИЙ ЦЕНТР РАЗВИТИЕ ОТ 7 МЕСЯЦЕВ ДО 7 ЛЕТ</h1>
						<h3>сделайте первые шаги к идеальному развитию своего ребенка!
						<br/>результат уже после 2 недель занятий!</h3>
					</div>

				</div>
				<section class="block-main">
					<div class="row">
						<div class="col-md-7">
							<iframe width="100%" height="360px" src="https://www.youtube.com/embed/M_Cvrxgce2w?modestbranding=1&autostart=1
&controls=0&showinfo=0&rel=0" frameborder="0" allowfullscreen style="border-bottom: #175f64 4px solid;"></iframe>
						</div>
						<div class="col-md-4 col-md-offset-1">

							<form class="forms">
								
								<div class="col-lg-12 col-md-12">
									<h4 class="center">ЗАПИШИТЕСЬ НА ПРОБНОЕ ЗАНЯТИЕ</h4>
								</div>
								<!-- <div class="col-lg-4 visible-lg">
									<h4 class="center">ОСТАВЬТЕ ЗАЯВКУ НА ПРОБНОЕ ПОСЕЩЕНИЕ 2 000 ТГ.</h4>
								</div> -->
								<div class="row">
									<div class="col-md-12">
									<input type="text" name="name" placeholder="Ваше имя" required>
								</div>
								
								<div class="col-md-12">
									<input type="text" class="telephone" name="telephone" placeholder="Ваш телефон" required>
								</div>
								<div class="col-md-12">
									<input type="email" name="email" placeholder="Ваш Email" required>
								</div>
								<div class="col-md-12">
									<div class="button-box">
									<button class="buttons">Отправить</button>
									</div>
								</div>
								</div>		
								<!-- Скрытые поля, которые содержат данные из UTM метки, если они небыли зашифрованы и название формы  -->
								<input type="hidden" name="formname" value="Заявка с верхней формы">
								<input type="hidden" name="source" class="source" value="<?php echo $_GET['utm_source'];?>" />
					            <input type="hidden" name="term" class="term" value="<?php echo $_GET['utm_term'];?>" />
							</form>
						</div>
					</div>
					<div class="row">
							<div class="adv-box">
								<div class="box">
									<b style="font-size:48px;">7</b>
									<p>дней в неделю</p>
								</div>
								<div class="box">
									<b style="font-size:48px;">12</b>
									<p>часов в день</p>
								</div>
								<div class="box">
									<b style="font-size:48px;">5</b>
									<p>учебных класса</p>
								</div>
								<div class="box">
									<b style="font-size:48px;">9</b>
									<p>программ развития ребенка</p>
								</div>
								<div class="box">
									<b style="font-size:48px;">6-8</b>
									<p>детей в группе</p>
								</div>
								<div class="box">
									<b style="font-size:48px;">5</b>
									<p>и более лет – опыт наших педагогов</p>
								</div>
							</div>
					</div>
				</section>
			</div>

		</div>

		<div class="arrow-wrap">
			<div class="arrow-bottom"></div>
		</div>

	</header>

	<section class="home section-if bg_gray">
		
		<div class="container">
			
			<div class="row">
				
				<div class="block-title col-md-10 col-md-offset-1">
					<h2>ЕСЛИ ВЫ СЕЙЧАС…</h2>
				</div>

			</div>

		</div>

		<div class="block-in">

			<div class="container">

				<div class="row">
					<div class="col-md-12">
						<div class="cards">

							<div class="card card-off">
								<p>Думаете о будущем ребенка, чтобы он вырос Счастливым, Умным, Здоровым и Успешным</p>
							</div>
							<div class="card card-off">
								<p>Хотите, чтобы малыш легко пошел в детский сад и с радостью учился в школе</p>
							</div>
							<div class="card card-off">
								<p>Не знаете и думаете, как развивать своего ребенка</p>
							</div>
							<div class="card card-off">
								<p>Хотите понять природные способности и таланты своего малыша, хотите знать, как их лучше раскрыть, чтобы помочь ребенку стать Успешнее и Счастливее в жизни</p>
							</div>
							<div class="card card-off">
								<p>Сравниваете развитие своего малыша с детьми своих знакомых и переживаете, что он еще не говорит, немного замкнут, не общается со сверстниками и т.д</p>
							</div>
							<div class="card card-off">
								<p>Вы ясно понимаете, что все это очень важно, Вы хотите найти ответы на эти вопросы и эффективные, оптимальные решения как можно скорей</p>
							</div>
						</div>
					</div>
				</div>

			</div>			

		</div>
		<div class="container">
			<div class="row">
				<div class="block-title col-md-10 col-md-offset-1">
					<h2>ОТКРОЙТЕ ОДИН ГЛАВНЫЙ СЕКРЕТ УСПЕШНОГО ВОСПИТАНИЯ РЕБЕНКА!</h2>
				</div>
			</div>
			<div class="arrow-wrap-color">
				<a href="#advantages" class="smoothScroll">
					<div class="arrow-bottom-color"></div>
				</a>	
			</div>
		</div>

	</section>


	<section class="home section-why" id="advantages" name="advantages">
		
		<div class="container">
			
			<div class="row">
				<div class="block-title col-md-10 col-md-offset-1">
					<h2>УСПЕЙТЕ ЗАЛОЖИТЬ ФУНДАМЕНТ ДЛЯ БОЛЕЕ БЫСТРОГО И ГЛУБОКОГО ИНТЕЛЛЕКТУАЛЬНОГО РАЗВИТИЯ СВОЕГО РЕБЕНКА</h2>
				</div>
				<div class="col-md-8">
					<div class="block-title col-md-10 col-md-offset-1">
						<h2>ПОЧЕМУ ДЕТИ В ВОСТОРГЕ ОТ CMG KIDS?</h2>
					</div>
					<div class="why-wrap">

						<div class="why-items">

								<div class="why-items-container">

									<div class="why-item-wrap">
										<a class="why-item">
											<span class="img-wrap">
												<img src="img/icons/section-why/1.png" alt="КАЧЕСТВО ЗАНЯТИЙ И РЕЗУЛЬТАТ КОНТРОЛИРУЕТ МЕТОДИСТ">
											</span>
											<span class="why-item-text">КАЧЕСТВО ЗАНЯТИЙ И РЕЗУЛЬТАТ КОНТРОЛИРУЕТ МЕТОДИСТ</span>
										</a>
									</div>
									<div class="why-item-wrap">
										<a class="why-item">
											<span class="img-wrap">
												<img src="img/icons/section-why/2.png" alt="НАШИ ПЕДАГОГИ ПОВЫШАЮТ КВАЛИФИКАЦИЮ КАЖДЫЙ МЕСЯЦ">
											</span>
											<span class="why-item-text">НАШИ ПЕДАГОГИ ПОВЫШАЮТ КВАЛИФИКАЦИЮ КАЖДЫЙ МЕСЯЦ</span>
										</a>
									</div>
									<div class="why-item-wrap">
										<a class="why-item">
											<span class="img-wrap">
												<img src="img/icons/section-why/6.png" alt="ЛЕГКО, ЧЕРЕЗ ИГРУ – РАЗВИВАЕМ РЕБЕНКА">
											</span>
											<span class="why-item-text">ЛЕГКО, ЧЕРЕЗ ИГРУ – РАЗВИВАЕМ РЕБЕНКА</span>
										</a>
										
									</div>
									<div class="why-item-wrap">
										<a class="why-item">
											<span class="img-wrap">
												<img src="img/icons/section-why/3.png" alt="ГОВОРИМ С ДЕТЬМИ НА ИХ ЯЗЫКЕ">
											</span>
											<span class="why-item-text">ГОВОРИМ С ДЕТЬМИ НА ИХ ЯЗЫКЕ</span>
										</a>
									</div>
									<div class="why-item-wrap">
										<a class="why-item">
											<span class="img-wrap">
												<img src="img/icons/section-why/4.png" alt="КОМАНДА ПРОФЕССИОНАЛОВ">
											</span>
											<span class="why-item-text">КОМАНДА ПРОФЕССИОНАЛОВ</span>
											
										</a>
										<p style="margin-top:-50px;">Опытная команда мастеров своего дела в которую входят педагоги – психологи, с опытом работы в детских садах, школах и детских центрах развития</p>
									</div>
									<div class="why-item-wrap">
										<a class="why-item">
											<span class="img-wrap">
												<img src="img/icons/section-why/5.png" alt="ЛУЧШИЕ МИРОВЫЕ МЕТОДИКИ">
											</span>
											<span class="why-item-text">ЛУЧШИЕ МИРОВЫЕ МЕТОДИКИ </span>
										</a>
										<p style="margin-top:-50px;">Домана-Маниченко, М. Монтессори, Никитиных, Петерсона, Кочемасовой, Колесниковой, Шевелевой, Железновой, Зайцева, Сухомлинский</p>
										
									</div>
									
								</div>

						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block-title col-md-10 col-md-offset-1">
						<h2>РЕЗУЛЬТАТ ЗАНЯТИЙ</h2>
						<h3>Готов к поступлению в топ-школы Астаны</h3>
						<h3>Обладает знаниями, превышающими базовый уровень</h3>
						<h3>Владеет лидерскими навыками: целеустремлен, самостоятелен и коммуникабелен</h3>
					</div>
				</div>
			</div>
			<div class="arrow-wrap-color">
				<a href="#programs" class="smoothScroll">
					<div class="arrow-bottom-color"></div>
				</a>	
			</div>
		</div>

	</section>
	
	<section class="home section-program" id="programs" name="programs">
		
		<div class="container">
			
			<div class="row">
				<div class="block-title col-md-10 col-md-offset-1">
						<h2>НАШИ ПРОГРАММЫ</h2>
				</div>
				<div class="col-md-6 center">
					<h3>ЛЮБОЗНАЙКА<br/>от 7 до 18 месяцев</h3>
					<img src="img/program/p1.jpg" class="img-responsive">
					<div class="clear"><br/></div>
					<div class="visible-sm visible-xs">
						<p>Занятие состоит из блоков, каждый из которых длится 10-15 минуты. Упражнения чередуются с играми на ковре, музыкальными и динамическими паузами. Каждый ребенок занимается вместе с мамой или другим взрослым человеком (папой, няней, бабушкой).
						</p>
						<p>Продолжительность занятия – 60 минут.  Максимальное количество детей в группе – 8.</p>
						<h4 style="color:red;">2 или 3 раза в неделю</h4>
					</div>
					<a href="#callback" class="buttons visible-sm visible-xs">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>НЕПОСЕДА<br/> от 1,7 года до 2,5 лет</h3>
					<img src="img/program/p2.jpg" class="img-responsive">
					<div class="clear"><br/></div>
					<div class="visible-sm visible-xs">
						<p>Занятие состоит из блоков, каждый из которых длится 10-15 минуты. Упражнения чередуются с играми на ковре, музыкальными и динамическими паузами. Каждый ребенок занимается вместе с мамой или другим взрослым человеком (папой, няней, бабушкой).
						</p>
						<p>Продолжительность занятия – 90 минут.  Максимальное количество детей в группе – 8.</p>
						<h4 style="color:red;">2 или 3 раза в неделю</h4>
					</div>
					<a href="#callback" class="buttons visible-sm visible-xs">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-12 center visible-lg visible-md">
					<p>Занятие состоит из блоков, каждый из которых длится 10-15 минуты. <br/>Упражнения чередуются с играми на ковре, музыкальными и динамическими паузами. <br/>Каждый ребенок занимается вместе с мамой или другим взрослым человеком (папой, няней, бабушкой).
					</p>
					<div class="col-md-6"><p>Продолжительность занятия – 60 минут.  <br/>Максимальное количество детей в группе – 8.</p></div>
					<div class="col-md-6"><p>Продолжительность занятия – 90 минут.  <br/>Максимальное количество детей в группе – 8.</p></div>
					
					<h4 style="color:red;">2 или 3 раза в неделю</h4>
				</div>
				<div class="col-md-6 center visible-lg visible-md">
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center visible-lg visible-md">
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>ПОЧЕМУЧКА<br/>от 2,5 до 3,5 лет</h3>
					<img src="img/program/p3.jpg" class="img-responsive">
					<div class="clear"><br/></div>
					<div class="visible-sm visible-xs">
						<p>Уроки будут включать игры и упражнения, связанные с развитием речи, познанием окружающего мира, обучением счёту, чтению и грамоте, подготовкой руки к письму. Наши занятия позволят овладеть всеми необходимыми знаниями и навыками для прохождения школьного собеседования.</p>
						<p>Продолжительность занятия – 90 минут.  Максимальное количество детей в группе – 8.</p>
						<h4 style="color:red;">2 или 3 раза в неделю</h4>
					</div>
					<a href="#callback" class="buttons visible-sm visible-xs">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>ВСЕЗНАЙКА <br/> от 3,5 до 5 лет</h3>
					<img src="img/program/p4.jpg" class="img-responsive">
					<div class="clear"><br/></div>
					<div class="visible-sm visible-xs">
						<p>Уроки будут включать игры и упражнения, связанные с развитием речи, познанием окружающего мира, обучением счёту, чтению и грамоте, подготовкой руки к письму. Наши занятия позволят овладеть всеми необходимыми знаниями и навыками для прохождения школьного собеседования.</p>
						<p>Продолжительность занятия – 90 минут.  Максимальное количество детей в группе – 8.</p>
						<h4 style="color:red;">2 или 3 раза в неделю</h4>
					</div>
					<a href="#callback" class="buttons visible-sm visible-xs">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-12 center visible-lg visible-md">
					<p>Уроки будут включать игры и упражнения, связанные с развитием речи, познанием окружающего мира, обучением счёту, чтению и грамоте, подготовкой руки к письму. <br/>Наши занятия позволят овладеть всеми необходимыми знаниями и навыками для прохождения школьного собеседования.</p>
					<p>Продолжительность занятия – 90 минут.  Максимальное количество детей в группе – 8.</p>
					<h4 style="color:red;">2 или 3 раза в неделю</h4>
				</div>
				<div class="col-md-6 center visible-lg visible-md">
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center visible-lg visible-md">
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>МИНИ САД<br/>1,7 – 5 лет</h3>
					<img src="img/program/p5.jpg" class="img-responsive">
					<p>В нашем мини-садике работают две группы: <br/>
					<b>от 1,7 до 2,5 лет c 09-00 до 13-00 и<br/>
					 от 3 до 5 лет c 14-00 до 18-00</b><br/>
					Каждый день у детей занятия по расписанию: математика и чтение (по программе, соответствующей возрасту), развитие речи и мышления, музыка, физкультура, творчество.
					</p>
					<p>Продолжительность занятия – 4 часа.<br/>  
					Максимальное количество детей в группе – 8. 
					<div class="visible-lg visible-md"><br/></div>
					</p>
					<h4 style="color:red;">5 раз в неделю</h4>
					<div class="clear"><br/></div>
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>ТВОРЧЕСКАЯ МАСТЕРСКАЯ <br/>3,5 - 5 лет</h3>
					<img src="img/program/p6.jpg" class="img-responsive">
					<p>Во время творческих занятий ваш ребенок научится различать ощущения от окружающих предметов, их физические свойства: пластилин – мягкий, краски – вязкие, тесто – теплое. Не умея порой еще выразить свои чувства словами, ваш ребенок сможет выразить их в творчестве. Творческая мастерская – развитие художественного вкуса, воображения, восприятия, развитие нестандартного мышления и креативности. Особенно это важно для детей, когда творческое полушарие мозга развивается очень интенсивно и творческие занятия дают мощный толчок к развитию личности ребенка в целом.</p>
					<p>Продолжительность занятия – 60 минут.<br/>
					Максимальное количество детей в группе – 8. 
					</p>
					<h4 style="color:red;">2 или 3 раз в неделю</h4>
					<div class="clear"><br/></div>
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>ДОШКОЛЯТА <br/> 5 - 7 лет</h3>
					<img src="img/program/p7.jpg" class="img-responsive">
					<p>Уроки будут включать игры и упражнения, связанные с развитием речи, познанием окружающего мира, обучением счёту, чтению и грамоте, подготовкой руки к письму. Наши занятия позволят овладеть всеми необходимыми знаниями и навыками для прохождения школьного собеседования.</p>
					<p>Продолжительность занятия – 90 минут. <br/> Максимальное количество детей в группе – 8.
					<div class="visible-lg visible-md"><br/><br/></div>
					</p>
					<h4 style="color:red;">2, 3 или 5 раз в неделю</h4>
					<div class="clear"><br/></div>
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>АНГЛИЙСКИЙ ЯЗЫК <br/>4 - 7 лет</h3>
					<img src="img/program/p8.jpg" class="img-responsive">
					<p>Eye Level English Sparks  -  программа по изучению английского языка рассчитанная на детей начинающих его изучение. В процессе обучения по этой программе делается акцент на фонетику, распознавание речи, затем изучение алфавита, заучивание наиболее частых английских слов и выражений (за первый год их будет более 500 ). Изучение слов и выражений будет происходить в рамках описания каждодневных ситуаций окружающих ребенка, что будет способствовать быстрому и лучшему запоминанию их.</p>
					<p>Продолжительность занятия – 60 минут. <br/> Максимальное количество детей в группе – 8.</p>
					<h4 style="color:red;">2, 3 или 5 раз в неделю</h4>
					<div class="clear"><br/></div>
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>МАТЕМАТИКА
					<br/> 4-7 лет</h3>
					<img src="img/program/p9.jpg" class="img-responsive">
					<p>Play Math - специальная программа для самых маленьких детей, где малыши занимаются по разработанной для них программе. Броские и яркие интерактивные пособия и материалы знакомят детей с базовыми основами математики (больше-меньше, одинаковый - разный, различные фигуры, порядок, последовательность, пространственное мышление), изучают цифры от 1 до10, учатся считать.<br/>
Акцент делается на развитие мелкой моторики, на игровую форму подачи нового материала. Занятия по математике проходят одновременно на английском языке и дети приобретают начальный словарный запас в иностранном языке.
					</p>
					<p>Продолжительность занятия – 60 минут.<br/>  
					Максимальное количество детей в группе – 8. <br/>&nbsp;
					</p>
					<h4 style="color:red;">2, 3 или 5 раз в неделю</h4>
					<div class="clear"><br/></div>
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
				<div class="col-md-6 center">
					<h3>ИНДИВИДУАЛЬНЫЕ ЗАНЯТИЯ<br/> &nbsp;</h3>
					<img src="img/program/p10.jpg" class="img-responsive">
					<p>В организационной структуре индивидуальных занятий нашего центра ведущими являются взаимодействие в паре «педагог – ребенок». 
					В индивидуальное занятие входит математика, геометрия, задания на логику, постановка руки, развитие речи, навык публичного выступления и творчество, а в конце дается время для рефлексивных высказываний ребенка.<br/> Индивидуальное обучение обеспечивает ребенку индивидуальный темп. В зависимости от способностей вашего ребенка подбирается ритм занятий и методика подачи предметов. 
					Индивидуальные занятия помогают родителям быстрее определить, какое направление в будущем предпочесть для своего ребенка, кем он хочет стать.</p>
					<p>Продолжительность занятия – 60 минут.</p>
					<h4 style="color:red;">Частота занятий обсуждается индивидуально</h4>
					<div class="clear"><br/></div>
					<a href="#callback" class="buttons">ЗАПИСАТЬСЯ НА ПРОБНЫЙ УРОК</a>
				</div>
			</div>
			<br/>
			<div class="arrow-wrap-color">
				<a href="#reviews" class="smoothScroll">
					<div class="arrow-bottom-color"></div>
				</a>	
			</div>
	</div>

	</section>
	
	<section class="home"  id="reviews" name="reviews">
		
		<div class="container">
			
			<div class="row">
				
				<div class="block-title col-md-10 col-md-offset-1">
					<h2>Отзывы</h2>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="carousel slide" id="myCarousel">
				        <div class="carousel-inner" style=";margin-left: -23px;">
				            <div class="item active">
				                    <ul class="thumbnails">
				                        <li class="col-sm-3">
				    						<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r1.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Динара, мама Азины и Инкар</h4>
													<p>Мне и детям очень нравится. Увидела рекламу в Instagram, привлекли яркие занятия, советую всем подругам и родственникам!</p>
												</div>
				                            </div>
				                        </li>
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r2.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Мерей, мама Амирлана</h4>
													<p>Мы ходим в казахскую группу к Жадыре. Нам очень нравится, как она ведет урок. Очень ответственно относится к каждому занятию. Методика интересная. Я думаю, что здесь в CMG KIDS мой ребенок многому научится. Уже советую всем своим знакомым. Центр очень чистый и красочный.</p>
												</div>
				                            </div>
				                        </li>
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r3.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Nancy, Daniel’s mother</h4>
													<p>I was worried at the beginning that no one will speak English. Now I feel happy that the staff can speak English and Daniel is happy and feel like home. This is what I was looking for.</p>
												</div>
				                            </div>
				                        </li>
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r4.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Гульмира, мама Альнура.</h4>
													<p>Очень рада, что в городе открылся ваш центр. Уютно, чисто, отличные преподаватели, ребенку комфортно, сразу видны результаты. Спасибо за ваш труд и старания.</p>
												</div>
				                            </div>
				                        </li>
				                    </ul>
				              </div><!-- /Slide1 --> 
				            <div class="item">
				                    <ul class="thumbnails">
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r5.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Асель, мама Тимура.</h4>
													<p>Мой сын Тимур ходит в CMG центр с начала марта и будем продолжать ходить. Тимуру очень нравится, он всегда собирается в хорошем настроении и остается с удовольствием. Начинает произносить слова, учиться повторять пройденный материал дома. Желаю CMG центру процветания и всего наилучшего! Советую родителям записаться на пробные занятия, и вы увидите результаты с первого раза.</p>
												</div>
				                            </div>
				                        </li>
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r6.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Жадыра, няня Раяны</h4>
													<p>Мы с Раяной посещаем CMG больше двух недель. Раяна ходить в казахскую группу. Учебный процесс очень хорошо организован. Преподаватель отличный специалист в своем деле.</p>
												</div>
				                            </div>
				                        </li>
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r7.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Шахноза, няня Арлана</h4>
													<p>Мы ходим в CMG больше двух месяцев. Нам очень нравится в этом центре. Здесь Арлан научился считать. У нас очень хороший преподаватель. </p>
												</div>
				                            </div>
				                        </li>
				                        <li class="col-sm-3">
											<div class="fff">
												<div class="thumbnail">
													<a href="#"><img src="img/reviews/r8.jpg" alt=""></a>
												</div>
												<div class="caption">
													<h4>Венера, няня Назара</h4>
													<p>Мы ходим уже второй месяц в этот центр и нам очень нравится. Тут ребенку интересно и к нам хорошо относятся. Назар начал разговаривать и различать цвета. Я советую этот центр своим знакомым.</p>
												</div>
				                            </div>
				                        </li>
				                    </ul>
				              </div><!-- /Slide2 --> 
				        </div>
		        
					    <nav>
							<ul class="control-box pager">
								<li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
								<li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
							</ul>
						</nav>
					   <!-- /.control-box -->   
				    </div><!-- /#myCarousel -->	
				</div>
			</div>
			<div class="arrow-wrap-color">
				<a href="#contacts" class="smoothScroll">
					<div class="arrow-bottom-color"></div>
				</a>	
			</div>
		</div>
	</section>
	

	<section class="home section-contacts" id="contacts" name="contacts">

		<div class="container">

			<div class="block-in">

				<div class="row">

					<div class="col-md-8 s8-left">
						<div class="block-title">
							<h2>Контакты</h2>
						</div>
					</div>
					<div id="f_org-map0" class="contacts-astana">
						<div class="col-lg-12">
							<a class="dg-widget-link" href="http://2gis.kz/astana/firm/70000001018130347/center/71.46157979965211,51.1296533345414/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Астаны</a><div class="dg-widget-link"><a href="http://2gis.kz/astana/center/71.461616,51.128191/zoom/16/routeTab/rsType/bus/to/71.461616,51.128191╎Creative Minds Group, ТОО?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Creative Minds Group, ТОО</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":"100%","height":400,"borderColor":"#a3a3a3","pos":{"lat":51.1296533345414,"lon":71.46157979965211,"zoom":16},"opt":{"city":"astana"},"org":[{"id":"70000001018130347"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
						</div>
						<div class="col-lg-12">
							<h4>Как нас найти:</h4>
							<p>Мы расположены в центре столицы, на правом берегу неподалёку
							от набережной, мечети «Хазрет султан», посольства США.</p>
							<b>На автобусе:</b>
							<p>Остановка «Парк им. Бауыржана Момышулы» – 35,41,46.<br/>
							Остановка: «ЖК «Хайвилл Астана»» - 32,40</p>
							<b>На автомобиле:</b>
							<p>Двигаясь по улице Сарайшык, со стороны левого берега, в сторону правого берега, попадёте на пр. Кошкарбаева, с правой стороны увидите ЖК «Хайвилл Астана», доезжаете до блока С2.
							Двигаясь по проспекту Б. Момышулы, едете прямо, проезжая проспект Тауелсиздик, затем поворачиваете налево, на пр. Кошкарбаева, въезд в паркинг с правой стороны.</p>
							<b>Паркинг:</b>
							<p>1.	В блоке С 4 есть платная гостевая парковка (1 час бесплатно, далее 200 тг)<br/>
							2.	Перед блоком А находится бесплатная уличная парковка.</p>
						</div>
					</div>
					<div id="f_org-map1" class="contacts-orda">
						<div class="col-lg-12">
							<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=Nn6UXK1pbA0-BKvz33XyxVpNWLq-qjVS&amp;width=100%&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
						</div>
						<div class="col-lg-12">
							<h4>Как нас найти:</h4>
							<p>Мы расположены в центре города, неподалеку от центрального вокзала, рядом стадион "Локомотив".</p>
							<b>На автобусе:</b>
							<p>Автобусы # 11, 14, 7, 17 остановка<br/> ул. Сулейменова угол ул. Ауельбекова.</p>
							<b>На автомобиле:</b>
							<p>Двигаясь по улице Сулейменова, со стороны стадиона, в сторону вокзала, проезжаете Ресторан "Шейх" с правой стороны увидите наш центр . <br/>Рядом есть автомойка <Garage #1> и бесплатная парковка.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="any_q">
		
		<h2>Остались вопросы?</h2>
		<h3>ПОСЕТИТЕ БЕСПЛАТНУЮ КОНСУЛЬТАЦИЮ!</h3>
		<div class="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<form class="forms">
						<h4>ОСТАВИТЬ ЗАЯВКУ</h4>
						
						<label>
							<span>Ваше имя:</span>
							<input type="text" name="name" required>
						</label>

						<label>
							<span>Ваш телефон:</span>
							<input type="text" name="telephone" class="telephone" required>
						</label>
						<label>
							<span>Ваш email:</span>
							<input type="email" name="email" required>
						</label>
						
						<div class="button-wrap">
							<button class="buttons">Отправить</button>
						</div>
						<p><input type="checkbox" checked> Я согласен(а) получать рекламную информацию от CMG Kids</p>
						
						<input type="hidden" name="source" class="source" value="<?php echo $_GET['utm_source'];?>" />
				        <input type="hidden" name="term" class="term" value="<?php echo $_GET['utm_term'];?>" />
						<input type="hidden" name="formname" value="Заявка с нижней формы">
					</form>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>		
	</section>
	<footer class="home main-footer s_blue">
		
		<div class="container">
			
			<div class="row">
				
				<div class="footer-content">
					
					<div class="container">

						<div class="row">

							<div class="col-md-3 col-sm-6 col-xs-6">
								<a href="" class="logo"><img src="img/wlogo.png" alt="Creative Minds Group"></a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="mail">
									<a href="mailto:info@cm-group.kz">info@cm-group.kz</a>
								</div>
								<div class="social">
									<a href="https://www.facebook.com/cmgkz" target="_blank"><img src="img/social-icons/fb.png" alt="Creative Minds Group Facebook"/></a>&nbsp;
									<a href="http://vk.com/cmgkz"  target="_blank"><img src="img/social-icons/vk.png" alt="Creative Minds Group Vk"></a>&nbsp;
									<a href="https://instagram.com/cmgkz"  target="_blank"><img src="img/social-icons/instagram.png" alt="Creative Minds Group Instagram"></a>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="time">
									Часы работы:<br/>
									ПН-ПТ с 09.00 до 21.00<br/>
									СБ-ВС с 11.00 до 18.00<br/>
									<div id="f_org-address0" class="contacts-astana">
										Астана, Кошкарбаева 2<br/>
										ЖК Highvill Astana, Блок С-2
									</div>
									<div id="f_org-address1" class="contacts-orda">
										Кызылорда, С. Сулейменова 70
									</div>  
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">

								<div class="phone-wrap">	
									<div id="f_org-phone0" class="contacts-astana">
										<div class="phone"><span class="lptracker_phone">+7 7172 266 588</span></div>
										<div class="phone"><span class="lptracker_phone"><img src="img/whats.png"> +7 771 525 01 58</span></div><br/>
									</div>	
									<div id="f_org-phone1" class="contacts-orda">
										<div class="phone"><span class="lptracker_phone">+7 (701) 755 68 00</span></div>
									</div>
										<div class="app-version">
										<?php 
										if($_SESSION['desktopmode'] == 'true'){
											echo ('<a href="index.php?show_desktop_mode=false" style="color:white;">Адаптивная версия сайта</a>');
										}else {
											echo ('<a href="index.php?show_desktop_mode=true" style="color:white;" class="visible-xs visible-sm">Полная версия сайта</a>');
										}
										?>
										<br/>
											<!-- Yandex.Metrika informer -->
											<a href="https://metrika.yandex.ru/stat/?id=34736905&amp;from=informer"
											target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/34736905/3_0_40A6AEFF_20868EFF_0_pageviews"
											style="width:88px; height:31px; border:0; display:none;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:34736905,lang:'ru'});return false}catch(e){}" /></a>
											<!-- /Yandex.Metrika informer -->

											<!-- Yandex.Metrika counter -->
											<script type="text/javascript">
											    (function (d, w, c) {
											        (w[c] = w[c] || []).push(function() {
											            try {
											                w.yaCounter34736905 = new Ya.Metrika({
											                    id:34736905,
											                    clickmap:true,
											                    trackLinks:true,
											                    accurateTrackBounce:true,
											                    webvisor:true
											                });
											            } catch(e) { }
											        });

											        var n = d.getElementsByTagName("script")[0],
											            s = d.createElement("script"),
											            f = function () { n.parentNode.insertBefore(s, n); };
											        s.type = "text/javascript";
											        s.async = true;
											        s.src = "https://mc.yandex.ru/metrika/watch.js";

											        if (w.opera == "[object Opera]") {
											            d.addEventListener("DOMContentLoaded", f, false);
											        } else { f(); }
											    })(document, window, "yandex_metrika_callbacks");
											</script>
											<noscript><div><img src="https://mc.yandex.ru/watch/34736905" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
											<!-- /Yandex.Metrika counter -->
										
										<!-- Google Analytics -->
										<script>
										  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
										  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
										  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
										  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

										  ga('create', 'UA-76632362-1', 'auto');
										  ga('send', 'pageview');

										</script>
										<!-- /Google Analytics -->
										</div>
								</div>

							</div>

							

						</div>

					</div>

				</div>

			</div>

		</div>

	</footer>
	<a href="#" class="scrollup">Наверх</a>

<div class="hidden">
	
	<form class="forms" id="callback">

		<h4>Заявка:</h4>
		<p>Оставьте заявку и мы свяжемся с вами в течение 15 минут в рабочее время с 09:00 до 21:00 c пн-сб.</p>

		<label>
			<span>Ваше имя:</span>
			<input type="text" name="name" required>
		</label>

		<label>
			<span>Ваш телефон:</span>
			<input type="text" class="telephone" name="telephone" required>
		</label>

		<label>
			<span>Ваш Email:</span>
			<input type="email" name="email" required>
		</label>

		<div class="button-wrap">
			<button class="buttons">Отправить</button>
		</div>
		
		<!-- Скрытые поля, которые содержат данные из UTM метки, если они небыли зашифрованы и название формы  -->
		<input type="hidden" name="formname" value="Выпадающее окно">
        <input type="hidden" name="source" class="source" value="<?php echo $_GET['utm_source'];?>" />
        <input type="hidden" name="term" class="term" value="<?php echo $_GET['utm_term'];?>" />
	</form>

</div>
<div id="selectCityModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Спасибо за заявку!</h4>
            </div>
            <div class="modal-body">
				<div class="social" style="text-align:center;">
					<p>Специалист свяжется с Вами по<br/>
					контактам, которые Вы указали, для<br/>
					уточнения деталей в течение часа.<br/>
					Если это не произойдет, то, пожалуйста<br/>
					позвоните по телефонам<br/>
					<a href="tel:87172266588" class="phone">+7 7172 266 588</a><br/>
					или<br/>
					<a href="tel:87715250158" class="phone">+7 771 525 01 58</a><br/>
					А пока вы можете подписаться на<br/>
					аккаунты в соц. сетях</p>
					<a href="https://www.facebook.com/cmgkz" target="_blank"><img src="img/social-icons/fb.png" alt="Creative Minds Group Facebook"/></a>&nbsp;
					<a href="http://vk.com/cmgkz"  target="_blank"><img src="img/social-icons/vk.png" alt="Creative Minds Group Vk"></a>&nbsp;
					<a href="https://instagram.com/cmgkz"  target="_blank"><img src="img/social-icons/instagram.png" alt="Creative Minds Group Instagram"></a>
				</div>
            </div>
        </div>
    </div>
</div>
<!--[if lt IE 9]>
<script src="libs/html5shiv/es5-shim.min.js"></script>
<script src="libs/html5shiv/html5shiv.min.js"></script>
<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="libs/respond/respond.min.js"></script>
<![endif]-->
	
<!-- Load CSS -->
<script>
	//Load Libs CSS
	var ms=document.createElement("link");ms.rel="stylesheet";
	ms.href="libs/animate/animate.css";document.getElementsByTagName("head")[0].appendChild(ms);

	//Header Styles (compress & paste to header after release)
	// var ms=document.createElement("link");ms.rel="stylesheet";
	// ms.href="_header.min.css";document.getElementsByTagName("head")[0].appendChild(ms);

	//User Styles + Media Queries
	var ms=document.createElement("link");ms.rel="stylesheet";
	ms.href="css/main.css";document.getElementsByTagName("head")[0].appendChild(ms);
</script>

<script type="text/javascript">
	// Carousel Auto-Cycle
  $(document).ready(function() {
    $('.carousel').carousel({
      interval: 6000
    })
  });

</script>
<!-- Load Scripts -->
<script>var scr = {"scripts":[
	{"src" : "libs/modernizr/modernizr.js", "async" : false},
	{"src" : "libs/waypoints/waypoints.min.js", "async" : false},
	{"src" : "libs/animate/animate-css.js", "async" : false},
	{"src" : "libs/plugins-scroll/plugins-scroll.js", "async" : false},
	{"src" : "libs/equalHeight/jquery.equalheights.min.js", "async" : false},
	{"src" : "libs/drawsvg/drawfillsvg.min.js", "async" : false},
	{"src" : "libs/Magnific-Popup/jquery.magnific-popup.min.js", "async" : false},
	{"src" : "libs/owl/owl.carousel.min.js", "async" : false},
	{"src" : "js/mask.js", "async" : false},
	{"src" : "js/common.js", "async" : false}
	]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
</script>

</body>
</html>